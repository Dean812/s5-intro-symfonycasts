<?php


namespace App\Controller;


use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class CommentController extends AbstractController
{
    /**
     * @Route("/comments/{id}/vote/{direction<up|down>}", methods="POST")
     */
    public function CommentVote($id, $direction, LoggerInterface $logger)
    {
        // todo use id query database

        //use real logic here to save this to the database
        if ($direction === 'up') {
            $logger->info('voting up!');
            $currentVoteCount = rand(7, 100);
        } else {
            $logger->info('voting down!');
            $currentVoteCount = rand(0, 5);
        }

        // autre écriture : return new JsonResponse([ pareil ]);
        return $this->json(['votes' => $currentVoteCount]);

    }
}